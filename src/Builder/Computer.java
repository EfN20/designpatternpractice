package Builder;

public class Computer {
    private static long id_gen = 1;
    private long id;
    private String motherboardName;
    private String cpuName;
    private int ram;
    private int hddCapacity;
    private int wattPowerUnit;
    private String gpuName;

    public Computer(String motherboardName, String cpuName, int ram, int hddCapacity, int wattPowerUnit){
        generateId();
        setMotherboardName(motherboardName);
        setCpuName(cpuName);
        setRam(ram);
        setHddCapacity(hddCapacity);
        setWattPowerUnit(wattPowerUnit);
    }

    public Computer(String motherboardName, String cpuName, int ram, int hddCapacity,
                    int wattPowerUnit, String gpuName){
        generateId();
        setMotherboardName(motherboardName);
        setCpuName(cpuName);
        setRam(ram);
        setHddCapacity(hddCapacity);
        setWattPowerUnit(wattPowerUnit);
        setGpuName(gpuName);
    }

    public void generateId(){
        this.id = id_gen++;
    }

    public long getId() {
        return id;
    }

    public String getMotherboardName() {
        return motherboardName;
    }

    public void setMotherboardName(String motherboardName) {
        this.motherboardName = motherboardName;
    }

    public String getCpuName() {
        return cpuName;
    }

    public void setCpuName(String cpuName) {
        this.cpuName = cpuName;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getHddCapacity() {
        return hddCapacity;
    }

    public void setHddCapacity(int hddCapacity) {
        this.hddCapacity = hddCapacity;
    }

    public int getWattPowerUnit() {
        return wattPowerUnit;
    }

    public void setWattPowerUnit(int wattPowerUnit) {
        this.wattPowerUnit = wattPowerUnit;
    }

    public String getGpuName() {
        return gpuName;
    }

    public void setGpuName(String gpuName) {
        this.gpuName = gpuName;
    }

    public static class ComputerBuilder{
        private static long id_gen = 1;
        private long id;
        private String motherboardName;
        private String cpuName;
        private int ram;
        private int hddCapacity;
        private int wattPowerUnit;
        private String gpuName;

        public ComputerBuilder(){
            this.id = id_gen++;
        }

        public ComputerBuilder setMotherboard(String motherboardName){
            this.motherboardName = motherboardName;
            return this;
        }

        public ComputerBuilder setCPU(String cpuName){
            this.cpuName = cpuName;
            return this;
        }

        public ComputerBuilder setRAM(int ram){
            this.ram = ram;
            return this;
        }

        public ComputerBuilder setHDD(int hddCapacity){
            this.hddCapacity = hddCapacity;
            return this;
        }

        public ComputerBuilder setPowerUnit(int wattPowerUnit){
            this.wattPowerUnit = wattPowerUnit;
            return this;
        }

        public ComputerBuilder setGPU(String gpuName){
            this.gpuName = gpuName;
            return this;
        }

        public Computer build(){
            return new Computer(motherboardName, cpuName, ram, hddCapacity, wattPowerUnit, gpuName);
        }
    }

    @Override
    public String toString(){
        if(gpuName != null){
            return "ID of computer: " + id + " | motherboard is : " + motherboardName + " | CPU is: " + cpuName + " | RAM: " +
                    ram + "GB" + " | HDD capacity: " + hddCapacity + "GB" +
                    " | Power Unit: " + wattPowerUnit +"W" + " | GPU is: " + gpuName;
        }
        else{
            return "ID of computer: " + id + " | motherboard is : " + motherboardName + " | CPU is: " + cpuName + " | RAM: " +
                    ram + "GB" + " | HDD capacity: " + hddCapacity + "GB" +
                    " | Power Unit: " + wattPowerUnit +"W" + " | GPU is: not installed" ;
        }

    }
}

import Builder.Computer;
import Decorator.*;

public class Main {
    public static void main(String[] args) {
        //Builder
        Computer comp = new Computer.ComputerBuilder()
                .setMotherboard("Asus")
                .setCPU("I5-4690")
                .setRAM(8)
                .setHDD(1000)
                .setPowerUnit(600)
                .setGPU("GTX 980")
                .build();

        Computer compWithoutGPU = new Computer.ComputerBuilder()
                .setMotherboard("Gigabyte")
                .setCPU("I7-7700K")
                .setRAM(32)
                .setHDD(2000)
                .setPowerUnit(750)
                .build();
        System.out.println(comp);
        System.out.println();
        System.out.println(compWithoutGPU);
        //Builder
        System.out.println();
        //-----------------------------------------------------------------
        System.out.println();
        //Decorator
        IHouse basicHouse = new House();
        basicHouse.build();
        System.out.println("\n");
        IHouse MiddleTierHouse = new Garden(new Walls(new House()));
        MiddleTierHouse.build();
        System.out.println("\n");
        IHouse HighTierHouse = new Greenhouse(new Garage(new Garden(new Walls(new House()))));
        HighTierHouse.build();
        //Decorator
    }
}

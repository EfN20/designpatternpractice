package Decorator;

public class Greenhouse extends HouseDecorator{
    public Greenhouse(IHouse house) {
        super(house);
    }

    @Override
    public void build(){
        super.build();
        System.out.print("Installed greenhouse. ");
    }
}

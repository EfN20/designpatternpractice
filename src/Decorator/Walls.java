package Decorator;

public class Walls extends HouseDecorator{
    public Walls(IHouse house) {
        super(house);
    }

    @Override
    public void build(){
        super.build();
        System.out.print("Installed walls. ");
    }
}

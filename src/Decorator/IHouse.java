package Decorator;

public interface IHouse {
    void build();
}

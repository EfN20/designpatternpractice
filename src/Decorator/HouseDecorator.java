package Decorator;

public abstract class HouseDecorator implements IHouse{
    private IHouse house;

    public HouseDecorator(IHouse house){
        this.house = house;
    }

    @Override
    public void build(){
        this.house.build();
    }
}

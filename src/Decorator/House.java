package Decorator;

public class House implements IHouse{
    @Override
    public void build() {
        System.out.print("This is a private house. ");
    }
}

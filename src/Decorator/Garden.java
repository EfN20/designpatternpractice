package Decorator;

public class Garden extends HouseDecorator {
    public Garden(IHouse house) {
        super(house);
    }

    @Override
    public void build(){
        super.build();
        System.out.print("Installed garden. ");
    }
}

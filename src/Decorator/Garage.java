package Decorator;

public class Garage extends HouseDecorator {
    public Garage(IHouse house) {
        super(house);
    }

    @Override
    public void build(){
        super.build();
        System.out.print("Installed garage. ");
    }
}
